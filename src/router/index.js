import Vue from 'vue'
import VueRouter from 'vue-router'
import AboutView from '../views/AboutView.vue'
import ErrorView from '../views/ErrorView.vue'
import ProductDetailView from "@/views/ProductDetailView";
import ProductListView from "@/views/ProductListView";
import LoginView from "@/views/LoginView";
import UsersView from "@/views/UsersView";
import ShoppingCartView from "@/views/ShoppingCartView";
import store from "@/store/store";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'products',
        component: ProductListView
    },
    {
        path: '/product/:id/',
        name: 'product',
        component: ProductDetailView,
    },
    {
        path: '/shopping-cart',
        name: 'cart',
        component: ShoppingCartView,
    },
    {
        path: '/login/',
        name: 'login',
        component: LoginView,
    },
    {
        path: '/users/',
        name: 'users',
        component: UsersView,
        beforeEnter: (to, from, next) => {
            if (to.name !== 'login' && to.name === 'users' && !store.isStaff()) {
                next({name: 'products'})
            } else {
                next();
            }
        }
    },
    {
        path: '/about',
        name: 'About',
        component: AboutView
    },
    {
        path: '*',
        name: 'Error',
        component: ErrorView,
    },
];

export default routes;
