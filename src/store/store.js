import {cloneDeep, isNil} from 'lodash';

const store = {
    debug: true,
    state: {
        cart: [],
        user: null,
    },
    //user functions
    storeUser(user) {
        this.debugMessage('storeUser', user);
        this.state.user = user;
    },
    deleteUser() {
        this.debugMessage('deleteUser');
        this.state.user = null;
    },
    isStaff() {
        this.debugMessage('isStaff');
        if (!isNil(this.state.user)) {
            return this.state.user.is_staff;
        }
        return false;
    },
    isLogged() {
        this.debugMessage('isLogged');
        return this.state.user !== null;
    },

    addToCart(product) {
        this.debugMessage('addToCart', product)
        const existingProductIndex = this.state.cart.findIndex(element => element.id === product.id);
        if (existingProductIndex === -1) {
            let clonedProduct = cloneDeep(product);
            clonedProduct.quantity = 1;
            this.state.cart.push(clonedProduct);
        } else {
            this.state.cart[existingProductIndex].quantity = this.state.cart[existingProductIndex].quantity + 1;
        }
    },
    deleteFromCart(indexSearched) {
        this.debugMessage('deleteFromCart')
        this.state.cart = this.state.cart.filter((product, index) => index !== indexSearched);
    },
    emptyCart() {
        this.debugMessage('emptyCart')
        this.state.cart = [];
    },

    debugMessage(method, value) {
        if (this.debug) {
            if (value !== undefined) {
                console.log(method + ' triggered with', value)
            } else {
                console.log(method + ' triggered')
            }
        }
    }
};

export default store;